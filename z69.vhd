library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity z69 is
port(
	clk : in std_logic;
	btnUp : in std_logic;
	btnDown : in std_logic;
	o : out std_logic );
end z69;

architecture Behavioral of z69 is
	signal sad, max : std_logic_vector (1 downto 0);
begin
	process (clk)
	begin
		if (clk = '1') then
			sad <= sad + 1;
		end if;
	end process;
	process (btnUp, btnDown)
	begin
		if (btnUp = '1' and btnDown = '0' and max < "11") then
			max <= max + 1;
		elsif (btnDown = '1' and btnUp = '0' and max > "00") then
			max <= max - 1;
		end if;
	end process;
	process (sad, max)
	begin
		if (sad >= max) then
			o <= '1';
		else
			o <= '0';
		end if;
	end process;
end Behavioral;