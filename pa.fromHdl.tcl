
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name z69 -dir "D:/Xilinx/Projects/z69/planAhead_run_1" -part xc6slx16csg324-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "z69.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {z69.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set_property top z69 $srcset
add_files [list {z69.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc6slx16csg324-3
